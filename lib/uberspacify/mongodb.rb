Capistrano::Configuration.instance.load do

  # callbacks
  after   'deploy:setup',       	'mongo:setup_database_and_config'
  after   'deploy:finalize_update',	'mongo:configure_project_for_database'

  set(:mongoid_folder) { "#{fetch :shared_path}/config" }
  set(:mongoid_path) { "#{fetch :mongoid_folder}/mongoid.yml" }
  set(:mongocnf_path) { "#{fetch :home}/.mongo.cnf" }
  # custom recipes
  namespace :mongo do
    task :setup_database_and_config do
      # ensure that mongo is not already set up
      unless capture("if [ -e '#{fetch :mongoid_path}' ]; then echo -n 'true'; fi") == 'true'
        # set up mongo db and capture admin access information
        admin_access = capture('uberspace-setup-mongodb | grep Portnum# -A 2')
        config = {}
        config[:password] = admin_access[/Password:.+/]['Password: '.length..-2] # cut the \r that is captured at end of line
        #config[:password] = admin_access[/Password:.+/]['Password: '.length..-1]
        config[:port] = Integer(admin_access[/[0-9]{5}/]) 
        config[:user] = admin_access[/\S+_mongoadmin/]
        # save admin password to .mongo.cnf file
        put config.to_yaml, mongocnf_path
        # generate a db user password and the db user
        user_password = (0...8).map{ (('a'..'z').to_a+('A'..'Z').to_a)[rand(54)] }.join
        run(
          "mongo admin --port #{config[:port]}" +
          " -u #{config[:user]}" +
          " -p #{config[:password]}" +
          " --eval \"db.getSiblingDB('pixel-to-go')" +
          ".addUser({user:'locomotive'," +
          "pwd:'#{user_password}'," +
          "roles:['readWrite','dbAdmin']});\""
        )
        # write to $HOME/mongoid.yml
        mongoid = {'production' => {'clients' => {'default' => {} }}}
        access_data = mongoid['production']['clients']['default']
        access_data['database'] = 'pixel-to-go'
        access_data['hosts'] = ["localhost:#{config[:port]}"]
        access_data['options'] = {
          :user => 'locomotive',
          :password => user_password
        }
        run "mkdir #{ fetch :mongoid_folder }"
        put mongoid.to_yaml, mongoid_path
      end
    end

    task :configure_project_for_database do
      if capture("if [ -e '#{fetch :mongoid_path}' ]; then echo -n 'true'; fi") == 'true'
        run "ln -nfs #{fetch :mongoid_path} #{release_path}/config/mongoid.yml"
      end
    end
  
  end

end
